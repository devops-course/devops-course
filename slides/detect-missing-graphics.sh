#!/usr/bin/env sh
set -u
if [ "$#" -ne 1 ]; then
    echo "usage: $0 <context-log-file>"
    exit 1
fi

log_file=$1
exit_code=0

cannot_be_resolved=$(grep -E 'graphics.*inclusion.*cannot be resolved' ${log_file})
grep_exit_code=$?
if [ ${grep_exit_code} -eq 2 ]; then
    exit 1
fi

cannot_be_resolved_count=$(echo -n "${cannot_be_resolved}" | wc -l)
if [ ${cannot_be_resolved_count} -gt 0 ]; then
    exit_code=1
    echo "${cannot_be_resolved}"
fi

exit ${exit_code}
